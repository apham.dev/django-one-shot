from django.shortcuts import render, get_object_or_404
from todos.models import TodoList, TodoItem

# Create your views here.
def todo_list_list(request):
    all_the_lists = TodoList.objects.all()
    context = {"all_the_lists": all_the_lists}
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    # context = {"todo_object": todo}
    return render(request, "todos/detail.html", {"todo_list": todo_list})
